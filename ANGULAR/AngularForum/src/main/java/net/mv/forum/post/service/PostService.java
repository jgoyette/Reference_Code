package net.mv.forum.post.service;

import net.mv.forum.post.dto.PostDto;

public interface PostService {
	
	public PostDto addPost(PostDto post);

}
