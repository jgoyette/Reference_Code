package net.mv.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path(value = "resource1")
public class MyResource {

	@Path(value = "test")
	@GET
	public String testThis() {
		return "Testing.....";
	}

	@Path(value = "student")
	@GET
	@Produces(value={MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Student showStudent() {

		return new Student("John", 3.8, "Bio");

	}
	
	@Path(value="register")
	@POST
	@Consumes(value={MediaType.APPLICATION_JSON})
	public void registerStudent(Student student){
		System.out.println(student);
	}

}
