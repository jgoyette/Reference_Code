package net.mv.book;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

public class BookClient {

	public static void main(String[] args) {

		/*
		 * Configure a new Jersey Client using the ClienBuilder API.
		 */
		Client client = ClientBuilder.newClient(new ClientConfig());

		/*
		 * Retrieve a Book object from the getBestSeller() method in our
		 * webservice. Give it an URI (location), media type, and Class to map
		 * the incoming JSON object to.
		 */
		Book bestSeller = client
				.target("http://localhost:9090/BookStore/rest/book/bestseller")
				.request(MediaType.APPLICATION_JSON).get(Book.class);

		System.out.println(bestSeller);

		/*
		 * Retrieve a Response from the modifyBook(Book book) method of our
		 * service. We do this by posting a Book object to the service, and we
		 * print the modified value after we read it from the Response body.
		 */
		Response response = client
				.target("http://localhost:9090/BookStore/rest/book/modifyBook")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(bestSeller, MediaType.APPLICATION_JSON));

		/*
		 * Call the .readEntity(Class<T>.class) method to convert our Response
		 * body to a readable object.
		 */
		System.out.println("New Title "
				+ response.readEntity(Book.class).getTitle());

		/*
		 * Retrieve a Response from the buildResponse() method of our service.
		 * We do this by getting a Book object that is contained inside a Response object.
		 * We then determine what to do with the response based on the status code.
		 */
		Response response2 = client
				.target("http://localhost:9090/BookStore/rest/book/idealResponse/Tom")
				.request(MediaType.APPLICATION_JSON).get();

		if(response2.getStatus() == 402){
			System.out.println("There has been an error with the client's request!!!!");
		}else{
			System.out.println("All good...");
		}
		
	}

}
