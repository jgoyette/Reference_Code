package net.mv;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("book")
public class BookResource {

	@Path("bestseller")
	@GET
	@Produces(value = { MediaType.APPLICATION_JSON })
	public Book getBestSeller() {
		return new Book("Something", "Someone", 444, 5.0);
	}

	@Path("byAuthor/{author}")
	@GET
	@Produces(value = { MediaType.APPLICATION_JSON })
	public Book getBookByAuthor(@PathParam(value = "author") String author) {
		return new Book("New Book", author, 233, 1.0);
	}

	@Path("idealResponse/{author}")
	@GET
	@Produces(value = { MediaType.APPLICATION_JSON })
	public Response buildResponse(@PathParam(value = "author") String author) {

		Response response = null;

		Book book = new Book("Title", author, 1, 0.0);

		if (book.getAuthor().equals("Tom")) {
			response = Response.status(402).entity(book).build();
		} else {
			response = Response.status(200).entity(book).build();
		}
		return response;
	}

	@Path("modifyBook")
	@POST
	@Produces(value = { MediaType.APPLICATION_JSON })
	public Response modifyBook(Book book) {

		System.out.println(book);

		book.setTitle("MODIFIED");

		return Response.accepted().entity(book).build();

	}

}









