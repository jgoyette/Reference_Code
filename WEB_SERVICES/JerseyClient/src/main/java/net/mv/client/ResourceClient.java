package net.mv.client;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ResourceClient {

	public static void main(String[] args) {
		
		Response response = ClientBuilder.newClient().target("http://localhost:8080/JerseyService/rest/resource1/student")
					.request(MediaType.APPLICATION_JSON).get();
		
		Student student = response.readEntity(Student.class);
		
		System.out.println(student);
		
	}
	
}
