package net.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * Class that is used to map the result Set to a Person 
 * object when retrieving from the DB.
 * @author Jeff
 *
 */
public class PersonRowMapper implements RowMapper<Person>{

	@Override
	public Person mapRow(ResultSet rs, int row) throws SQLException {
		return new Person(rs.getString(1), rs.getString(2));
	}

}
