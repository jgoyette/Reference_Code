package net.mv.driver;

import net.mv.domain.Meter;
import net.mv.service.MeterService;
import net.mv.util.InsertData;

public class ExerciseDriver {
	
	public static void main(String[] args) {
		/*
		 * Insert data into the database prior to manipulation
		 */
		InsertData id = new InsertData();
		id.insertData();
		
		/*
		 * Meter service class to perform business logic
		 */
		MeterService service = new MeterService();
		
		/*
		 * Find random meter to get highest and average by
		 */
		Meter exMeter = service.findRandomMeter();
		
		
		/*
		 * Find highest meter by KWH
		 */
		System.out.println("Highest: " + service.findHighestKWH());
		
		/*
		 * Find average KWH for random meter
		 */
		System.out.println("Average: " + service.findAverageForMeter(exMeter));
		
		/*
		 * For the same random meter update the KWH with a random value
		 */
		service.updateKWH(exMeter);
		
	}

}
