package net.mv.service;

import java.util.List;

import net.mv.domain.Meter;
import net.mv.domain.MeterReading;
import net.mv.repo.MeterReadingRepo;
import net.mv.repo.MeterRepo;

import org.bson.Document;
import org.bson.types.ObjectId;

public class MeterService {

	private MeterRepo meterRepo = new MeterRepo();
	private MeterReadingRepo meterReadingRepo = new MeterReadingRepo();
	
	public void updateKWH(Meter meter){
		
		List<ObjectId> objectIds = meterReadingRepo.findObjectIdForRandomReading(meter);
		
		ObjectId id = objectIds.get((int) (Math.random()*objectIds.size()));
		
		double randomKWH = Math.random() * 1.5;
		
		meterReadingRepo.updateKWH(id, randomKWH);
		
	}

	public Meter findHighestKWH() {

		Document meterReadingDocument = meterReadingRepo.findByCriteria(
				new Document("kwh", -1)).first();



		Meter meter = meterRepo.findOne(new ObjectId(meterReadingDocument
				.getString("meter_id")));

		return meter;
	}

	public double findAverageForMeter(Meter meter) {

		List<MeterReading> meterReadings = meterReadingRepo
				.findAllForMeter(meter);

		double average = 0;

		for (MeterReading s : meterReadings) {
			average += s.getKwh();
		}

		average /= meterReadings.size();

		return average;
	}

	public Meter findRandomMeter() {

		List<Meter> meters = meterRepo.findAll();

		Meter meter = meters.get((int) (Math.random() * meters.size()));
		
		return meter;
	}
}
