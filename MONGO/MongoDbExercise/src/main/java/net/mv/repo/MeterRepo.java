package net.mv.repo;

import java.util.ArrayList;
import java.util.List;

import net.mv.domain.Address;
import net.mv.domain.Meter;
import net.mv.util.MongoUtil;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class MeterRepo {

	private MongoUtil util = MongoUtil.getUtil();

	private MongoDatabase db = util.getDb();

	public void insertOne(Meter meter) {
		Document meterDocument = new Document()
				.append("meterModel", meter.getMeterModel())
				.append("installDate", meter.getInstallDate())
				.append("address",
						new Document()
								.append("street",
										meter.getAddress().getStreet())
								.append("city", meter.getAddress().getCity())
								.append("state", meter.getAddress().getState())
								.append("zip", meter.getAddress().getZip()));

		db.getCollection("meters").insertOne(meterDocument);

	}

	public Meter findOne(ObjectId id) {

		Document doc = db.getCollection("meters").find(new Document("_id", id))
				.limit(1).first();

		Document addressDoc = (Document) doc.get("address");

		Address address = new Address(addressDoc.getString("street"),
				addressDoc.getString("city"), addressDoc.getString("state"),
				addressDoc.getInteger("zip", 0));

		Meter meter = new Meter(doc.getObjectId("_id").toString(),
				doc.getString("meterModel"), doc.getDate("installDate"),
				address);

		return meter;
	}

	public List<Meter> findAll() {

		List<Meter> meters = new ArrayList<Meter>();

		MongoCursor<Document> cursor = db.getCollection("meters").find()
				.iterator();

		while (cursor.hasNext()) {

			Document doc = cursor.next();

			Document addressDoc = (Document) doc.get("address");

			Address address = new Address(addressDoc.getString("street"),
					addressDoc.getString("city"),
					addressDoc.getString("state"), addressDoc.getInteger("zip",
							0));

			Meter meter = new Meter(doc.getObjectId("_id").toString(),
					doc.getString("meterModel"), doc.getDate("installDate"),
					address);
			meters.add(meter);
		}

		return meters;

	}

}
