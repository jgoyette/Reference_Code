package net.mv.repo;

import java.util.ArrayList;
import java.util.List;

import net.mv.domain.Meter;
import net.mv.domain.MeterReading;
import net.mv.util.MongoUtil;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class MeterReadingRepo {

	private MongoUtil util = MongoUtil.getUtil();

	private MongoDatabase db = util.getDb();
	/*
	 * Get a random meter reading for a given meter out of 24 meter readings
	 */
	public List<ObjectId> findObjectIdForRandomReading(Meter meter) {

		MongoCursor<Document> cursor = db.getCollection("meterReadings")
				.find(new Document("meter_id", meter.getMeterId())).iterator();

		List<ObjectId> objectIds = new ArrayList<ObjectId>();

		while (cursor.hasNext()) {

			Document doc = cursor.next();

			objectIds.add(doc.getObjectId("_id"));

		}
		return objectIds;
	}
	
	/*
	 * Get a list of all meter readings for a particular meter
	 */
	public List<MeterReading> findAllForMeter(Meter meter) {
		List<MeterReading> meterReadings = new ArrayList<MeterReading>();

		MongoCursor<Document> cursor = db.getCollection("meterReadings")
				.find(new Document("meter_id", meter.getMeterId())).iterator();

		while (cursor.hasNext()) {

			Document doc = cursor.next();


			MeterReading meterReading = new MeterReading(meter,
					doc.getDate("readingTime"), doc.getDouble("kwh"));


			meterReadings.add(meterReading);

		}

		return meterReadings;
	}

	public FindIterable<Document> findByCriteria(Document criteria) {
		FindIterable<Document> iterable = db.getCollection("meterReadings")
				.find().sort(criteria);
		return iterable;
	}
	
	public void insertMeterReading(MeterReading reading) {
		Document meterReading = new Document()
				.append("meter_id",
						new ObjectId(reading.getMeter().getMeterId()))
				.append("readingTime", reading.getReadingTime())
				.append("kwh", reading.getKwh());

		db.getCollection("meterReadings").insertOne(meterReading);
	}

	public void updateKWH(ObjectId id, double kwh) {

		System.out.println("Updated Reading Id: "+id);
		System.out.println("New KWH value" + kwh);
		db.getCollection("meterReadings").updateOne(new Document("_id", id),
				new Document("$set", new Document("kwh", kwh)));
	}
}
