package net.mv.util;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoUtil {

	private static MongoUtil util;

	private MongoClient mongo;

	private MongoDatabase db;

	private MongoUtil() {
		mongo = new MongoClient("localhost", 27017);
		db = mongo.getDatabase("test");
	}

	public static MongoUtil getUtil() {
		if (util == null) {
			util = new MongoUtil();
		}
		return util;
	}

	public MongoDatabase getDb() {
		return db;
	}

}
