package net.mv.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import org.bson.Document;

import net.mv.domain.Address;
import net.mv.domain.Meter;
import net.mv.domain.MeterReading;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class InsertData {
	public void insertData() {

		MongoClient mongo = new MongoClient("localhost", 27017);

		MongoDatabase db = mongo.getDatabase("test");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",
				Locale.ENGLISH);

		Meter[] meters = new Meter[10];

		for (int i = 0; i < meters.length; i++) {
			meters[i] = new Meter("Fancy", new Date(), new Address(
					"111 Easy Way", "Alexandria", "VA", 22310));

			Document meteriDocument = new Document()
					.append("meterModel", meters[i].getMeterModel())
					.append("installDate", meters[i].getInstallDate())
					.append("address",
							new Document()
									.append("street",
											meters[i].getAddress().getStreet())
									.append("city",
											meters[i].getAddress().getCity())
									.append("state",
											meters[i].getAddress().getState())
									.append("zip",
											meters[i].getAddress().getZip()));

			db.getCollection("meters").insertOne(meteriDocument);

			meters[i].setMeterId(meteriDocument.getObjectId("_id").toString());

			System.out.println(meters[i].getMeterId());

			for (int j = 0; j < 24; j++) {
				Random r1 = new Random();
				double randomKWH = 1 + (4) * r1.nextDouble();
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.HOUR, 1);
				System.out.println("Updated  = " + cal.getTime());

				MeterReading meterReading1 = new MeterReading(meters[i],
						new Date(), randomKWH);

				Document meterReading1Document = new Document()
						.append("meter_id", meters[i].getMeterId())
						.append("readingTime", cal.getTime())
						.append("kwh", meterReading1.getKwh());

				db.getCollection("meterReadings").insertOne(
						meterReading1Document);
			}

		}
	}
}
