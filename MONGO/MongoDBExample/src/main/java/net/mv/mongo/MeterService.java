package net.mv.mongo;

import java.util.ArrayList;
import java.util.List;

import net.mv.domain.Address;
import net.mv.domain.Meter;
import net.mv.domain.MeterReading;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class MeterService {

	private MongoClient mongo = new MongoClient("localhost", 27017);

	private MongoDatabase db = mongo.getDatabase("test");

	public List<Meter> getAllMeters() {

		// FindIterable<Document> iterable = db.getCollection("meters").find();

		List<Meter> meters = new ArrayList<Meter>();

		MongoCursor<Document> cursor = db.getCollection("meters").find()
				.iterator();

		while (cursor.hasNext()) {

			Document doc = cursor.next();

			Document addressDoc = (Document) doc.get("address");

			Address address = new Address(addressDoc.getString("street"),
					addressDoc.getString("city"),
					addressDoc.getString("state"), addressDoc.getInteger("zip",
							0));

			Meter meter = new Meter(doc.getObjectId("_id").toString(),
					doc.getString("meterModel"), doc.getDate("installDate"),
					address);
			// System.out.println(doc);
			System.out.println(meter);
			meters.add(meter);
		}

		return meters;
	}

	public List<MeterReading> getAllMeterReadingsForMeter(Meter meter) {
		List<MeterReading> meterReadings = new ArrayList<MeterReading>();

		MongoCursor<Document> cursor = db.getCollection("meterReadings")
				.find(new Document("meter_id", meter.getMeterId())).iterator();

		while (cursor.hasNext()) {

			Document doc = cursor.next();

			System.out.println(doc);

			MeterReading meterReading = new MeterReading(meter,
					doc.getDate("readingTime"), doc.getDouble("kwh"));

			System.out.println(meterReading);

			meterReadings.add(meterReading);

		}
		return meterReadings;
	}

	public Meter getHighestMeter() {
		Meter meter = null;

		FindIterable<Document> iterable = db.getCollection("meterReadings")
				.find().sort(new Document("kwh", -1)).limit(1);

		Document meterReadingDoc = iterable.first();

		System.out.println(meterReadingDoc);
		
		System.out.println(meterReadingDoc.getString("meter_id"));

		Document doc = db
				.getCollection("meters")
				.find(new Document("_id", new ObjectId(meterReadingDoc.getString("meter_id"))))
				.limit(1).first();

		Document addressDoc = (Document) doc.get("address");

		Address address = new Address(addressDoc.getString("street"),
				addressDoc.getString("city"), addressDoc.getString("state"),
				addressDoc.getInteger("zip", 0));

		meter = new Meter(doc.getObjectId("_id").toString(),
				doc.getString("meterModel"), doc.getDate("installDate"),
				address);
		// System.out.println(doc);
		System.out.println(meter);

		return meter;
	}
}
