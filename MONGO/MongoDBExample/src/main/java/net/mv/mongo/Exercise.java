package net.mv.mongo;

import java.util.ArrayList;
import java.util.List;

import net.mv.domain.Meter;
import net.mv.domain.MeterReading;


public class Exercise {
	public static void main(String[] args) {
		
		
		MeterService meterService = new MeterService();
		
		
		
		List<Meter> meters = meterService.getAllMeters();
		
		List<MeterReading> meterReadings = meterService.getAllMeterReadingsForMeter(meters.get(0));
		
		double average = 0;
		
		for(MeterReading s: meterReadings){
			average += s.getKwh();
		}
		
		average /= meterReadings.size();
		System.out.println(average);
		
		System.out.println("Highest Meter : " + meterService.getHighestMeter());
		
//		MeterReading maxKWHReading = null;
//		
//		for(MeterReading meterReading : meterReadings){
//			
//		}
		
//		MongoClient mongo = new MongoClient("localhost", 27017);
//
//		MongoDatabase db = mongo.getDatabase("test");
//		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",
//				Locale.ENGLISH);
//
//		Meter meter1 = new Meter("Fancy", new Date(), new Address(
//				"111 Easy Way", "Alexandria", "VA", 22310));

		// Document meter1Document = new Document()
		// .append("meterModel", meter1.getMeterModel())
		// .append("installDate", meter1.getInstallDate())
		// .append("address",
		// new Document()
		// .append("street",
		// meter1.getAddress().getStreet())
		// .append("city", meter1.getAddress().getCity())
		// .append("state", meter1.getAddress().getState())
		// .append("zip", meter1.getAddress().getZip()));
		//
		// db.getCollection("meters").insertOne(meter1Document);
		//
		// meter1.setMeterId(meter1Document.getObjectId("_id").toString());

//		System.out.println(meter1.getMeterId());
//
//		MeterReading meterReading1 = new MeterReading(meter1, new Date(), 4);
//
//		Document meterReading1Document = new Document()
//				.append("meter_id", "55fae2cfdeefef18b003ee19")
//				.append("readingTime", new Date())
//				.append("kwh", meterReading1.getKwh());
//
//		db.getCollection("meterReadings").insertOne(meterReading1Document);
//
//		FindIterable<Document> iterable = db.getCollection("meterReadings")
//				.find(new Document().append("meter_id",
//						"55fae2cfdeefef18b003ee19"));
//
//		iterable.forEach(new Block<Document>() {
//
//			public void apply(Document arg0) {
//
//				System.out.println(arg0);
//
//			}
//		});

	}
}
