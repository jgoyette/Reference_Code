package net.mv.domain;

import java.util.Date;

public class MeterReading {

	private Meter meter;
	private Date readingTime;
	private double kwh;

	public MeterReading(Meter meter, Date readingTime, double kwh) {
		super();
		this.meter = meter;
		this.readingTime = readingTime;
		this.kwh = kwh;
	}

	public MeterReading() {
		super();
	}

	@Override
	public String toString() {
		return "MeterReading [meter=" + meter + ", readingTime=" + readingTime
				+ ", kwh=" + kwh + "]";
	}

	public Meter getMeter() {
		return meter;
	}

	public void setMeter(Meter meter) {
		this.meter = meter;
	}

	public Date getReadingTime() {
		return readingTime;
	}

	public void setReadingTime(Date readingTime) {
		this.readingTime = readingTime;
	}

	public double getKwh() {
		return kwh;
	}

	public void setKwh(double kwh) {
		this.kwh = kwh;
	}

}
