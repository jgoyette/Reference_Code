package net.mv;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoExample {

	public static void main(String[] args) throws ParseException {

		MongoClient mongo = new MongoClient("localhost", 27017);

		MongoDatabase db = mongo.getDatabase("test");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",
				Locale.ENGLISH);
		Document meter1 = new Document()
				.append("meterModel", "model 1")
				.append("installDate", format.parse("2014-10-01T00:00:00Z"))
				.append("address",
						new Document().append("stree", "111 Easy Way")
								.append("city", "Fairfax")
								.append("state", "VA").append("zip", "22030"));
		System.out.println(meter1.getObjectId("_id"));

		db.getCollection("meters").insertOne(meter1);

		System.out.println(meter1.getObjectId("_id"));

		Document meterReading1 = new Document()
				.append("meter_id", meter1.getObjectId("_id"))
				.append("readingTime", "2014-10-01T00:00:00Z")
				.append("kwh", "3.4");
		
		db.getCollection("meterReadings").insertOne(meterReading1);
		
		db.getCollection("meterReadings").insertOne(meter1);

		mongo.close();
	}
}
