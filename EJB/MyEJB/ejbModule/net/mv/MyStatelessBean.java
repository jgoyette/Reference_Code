package net.mv;

import javax.ejb.Stateless;

/**
 * Session Bean implementation class MyStatelessBean
 */
@Stateless(mappedName = "statelessBean")
public class MyStatelessBean implements MyStatelessBeanRemote {

    /**
     * Default constructor. 
     */
    public MyStatelessBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public String sayHello(String name) {
		System.out.println(name + " is calling my ejb.");
		return "Hello, " + name + " from Jeff";
	}

}
