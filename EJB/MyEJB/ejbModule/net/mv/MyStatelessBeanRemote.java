package net.mv;

import javax.ejb.Remote;

@Remote
public interface MyStatelessBeanRemote {
	
	public String sayHello(String name);

}
