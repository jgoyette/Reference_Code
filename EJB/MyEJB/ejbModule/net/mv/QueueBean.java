package net.mv;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Message-Driven Bean implementation class for: QueueBean
 */
@MessageDriven(
		activationConfig = { @ActivationConfigProperty(
				propertyName = "destination", propertyValue = "jms/queue"), @ActivationConfigProperty(
				propertyName = "destinationType", propertyValue = "javax.jms.Queue")
		}, 
		mappedName = "jms/queue")
public class QueueBean implements MessageListener {

    /**
     * Default constructor. 
     */
    public QueueBean() {
        // TODO Auto-generated constructor stub
    }
	
	/**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {

    	if(message instanceof TextMessage){
    		
    		TextMessage tm = (TextMessage) message;
    		
    		try {
				System.out.println("Message Contents: " + tm.getText());
				
				System.out.println("This: " + tm.getStringProperty("this"));
				System.out.println("That: " + tm.getStringProperty("that"));
				
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    	}
    	
    }

}
