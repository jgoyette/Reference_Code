package net.mv;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import weblogic.jndi.WLInitialContextFactory;

public class TestClient {

	public static void main(String[] args) {

		Properties props = new Properties();

		props.put(Context.PROVIDER_URL, "t3://192.168.20.56:7001");

		props.put(Context.INITIAL_CONTEXT_FACTORY,
				WLInitialContextFactory.class.getName());

		try {
			Context ctx = new InitialContext(props);

			MyStatelessBeanRemote msbr = (MyStatelessBeanRemote) ctx
					.lookup("statelessBean#"
							+ MyStatelessBeanRemote.class.getName());
			
			System.out.println(msbr.sayHello("Jeff"));

		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

}
