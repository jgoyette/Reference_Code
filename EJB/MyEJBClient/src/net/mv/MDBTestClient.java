package net.mv;

import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import weblogic.jndi.WLInitialContextFactory;

public class MDBTestClient {
	
	
	public static void main(String[] args) {
		
		Properties props = new Properties();
		
		props.put(Context.PROVIDER_URL, "t3://localhost:7001");
		props.put(Context.INITIAL_CONTEXT_FACTORY, WLInitialContextFactory.class.getName());
		
		try {
			Context ctx = new InitialContext(props);
			
//			TopicConnectionFactory tcf = (TopicConnectionFactory) ctx.lookup("weblogic.jms.ConnectionFactory");
//			
//			TopicConnection tc = tcf.createTopicConnection();
//			
//			Topic destinationTopic = (Topic) ctx.lookup("jms/topic");
//			
//			TopicSession tSession = tc.createTopicSession(false, 0);
//			
//			TopicPublisher tPublisher = tSession.createPublisher(destinationTopic);
//			
//			TextMessage tm1 = tSession.createTextMessage("blah blah, go ask Ankit for bagels");
//			
//			tPublisher.publish(tm1);
			
			QueueConnectionFactory qcf = (QueueConnectionFactory) ctx.lookup("weblogic.jms.ConnectionFactory");
			
			QueueConnection qc = qcf.createQueueConnection();
			
			Queue destinationQueue = (Queue) ctx.lookup("jms/queue");
			
			QueueSession qSession = qc.createQueueSession(false, 0);
			
			QueueSender qSender = qSession.createSender(destinationQueue);
			
			TextMessage tm2 = qSession.createTextMessage("Blah blah, no bagels :(");
			
			tm2.setStringProperty("this", "foo");
			tm2.setStringProperty("that", "bar");
			
			qSender.send(tm2);
			
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
	}

}






