package net.mv.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable()
			.authorizeRequests()
				.antMatchers("/admin**").hasRole("ADMIN")
				.antMatchers("/dba**").hasRole("DBA")
				.antMatchers("/","/welcome.html").hasRole("USER")
				.antMatchers("/login").permitAll()
			.and()
			.formLogin().successHandler(new AuthSuccessHandler())
			.and()
			.logout();
	
	}
	
	@Autowired
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth
			.inMemoryAuthentication()
			.withUser("Jeff").password("1234").roles("USER")
			.and()
			.withUser("Tom").password("1234").roles("ADMIN")
			.and()
			.withUser("Mike").password("1234").roles("DBA");
		
	}

}
