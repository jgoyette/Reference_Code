<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${ title }</title>
</head>
<body>

	<h1>Title: ${ title }</h1>
	<h1>Message: ${ message }</h1>

	<security:authorize access="hasRole('ROLE_USER')"><h1>WELCOME REGULAR USER!!!</h1></security:authorize>
	<security:authorize access="hasRole('ROLE_ADMIN')"><h1>WELCOME ADMIN!!!</h1></security:authorize>
	<security:authorize access="hasRole('ROLE_DBA')"><h1>WELCOME DBA!!!</h1></security:authorize>

	<a href="${ pageContext.request.contextPath }/logout">Logout</a>

</body>
</html>