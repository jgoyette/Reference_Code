package net.mv.security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {
	
	@RequestMapping(value={"/","/welcome**"}, method=RequestMethod.GET)
	public ModelAndView welcomePage(){
		
		ModelAndView model = new ModelAndView();
		model.addObject("title", "Welcome to Spring Security!!");
		model.addObject("message","This is the welcome page!");
		model.setViewName("hello");
		
		return model;
	}
	
	@RequestMapping(value={"/admin**"}, method=RequestMethod.GET)
	public ModelAndView viewAdminPage(){
		
		ModelAndView model = new ModelAndView();
		model.addObject("title", "Welcome to Spring Security!!");
		model.addObject("message", "This is the admin page!");
		model.setViewName("admin");
		
		return model;
	}
	
	@RequestMapping(value={"/dba**"}, method=RequestMethod.GET)
	public ModelAndView viewDbaPage(){
		
		ModelAndView model = new ModelAndView();
		model.addObject("title", "Welcome to Spring Security!");
		model.addObject("message", "Make a stored procedure....");
		model.setViewName("admin");
		
		return model;
	}

}
